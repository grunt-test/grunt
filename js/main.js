// Au lancement de la page, la banner de la section 2 est cachée;
var target = document.getElementById("banner");
target.classList.remove("bannerStyle");

// Pour afficher la banner lorsqu'un click est fait sur l'icon grunt;
document.getElementById('click-function').addEventListener('click', seeBanner, false);
function seeBanner(){
  target.classList.add('bannerStyle');
}
